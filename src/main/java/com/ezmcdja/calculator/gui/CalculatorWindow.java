package com.ezmcdja.calculator.gui;

import static com.ezmcdja.calculator.calculation.RomanNumeral.I;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ezmcdja.calculator.calculation.AnachronisticZeroException;
import com.ezmcdja.calculator.calculation.MathsOperator;
import com.ezmcdja.calculator.calculation.RomanCalculation;
import com.ezmcdja.calculator.calculation.RomanNumeral;

/**
 * Author: James McDermott.
 */
public class CalculatorWindow {
    private static final List<String> OPERATORS = MathsOperator.getOperatorSigns();
    private static final String FONT_NAME = "Arial";

    private static final Logger logger = LoggerFactory.getLogger(CalculatorWindow.class);

    private JFrame romanCalculatorFrame;
    private JTextField decimalField;
    private JTextField inputField;
    private JTextField resultField;

    private boolean nextIsNegative;
    private boolean operatorPresentAtEnd;

    /**
     * Create the application.
     */
    public CalculatorWindow() {
        initialise();
        operatorPresentAtEnd = true;
    }

    /**
     * Launch the application.
     */
    public static void main(final String[] args) {
        final Runnable runnable = () -> {
            try {
                new CalculatorWindow().romanCalculatorFrame.setVisible(true);
            } catch (final Exception e) {
                logger.error("{}", (Object[]) e.getStackTrace());
            }
        };
        EventQueue.invokeLater(runnable);
    }

    private void clearEverything() {
        inputField.setText("");
        resultField.setText("");
        decimalField.setText("");
        decimalField.setFont(new Font("Tahoma", Font.PLAIN, 11));
        decimalField.setBackground(new Color(240, 240, 240));
        operatorPresentAtEnd = true;
        nextIsNegative = false;
    }

    private void displayInvalidNumberMessage(final Exception anException) {
        decimalField.setBackground(Color.RED);
        decimalField.setFont(new Font(FONT_NAME, Font.BOLD, 30));
        decimalField.setText(anException.getMessage());
    }

    private void displayResults(final String decimalCalculation, final String romanResult) {
        resultField.setText(romanResult);

        decimalField.setFont(new Font("Tahoma", Font.PLAIN, 11));
        decimalField.setBackground(new Color(240, 240, 240));
        decimalField.setText(decimalCalculation);
    }

    private JTextField doCommonFieldInitialisation() {
        final JTextField field = new JTextField();
        field.setEnabled(false);
        field.setPreferredSize(new Dimension(510, 40));
        field.setMinimumSize(new Dimension(510, 20));
        field.setFont(new Font(FONT_NAME, Font.BOLD, 30));
        field.setDisabledTextColor(Color.BLACK);
        field.setBackground(Color.WHITE);
        field.setColumns(10);
        field.setBounds(10, 1, 490, 40);
        return field;
    }

    private JPanel doCommonFieldPanelInitialisation() {
        final JPanel inputFieldPanel = new JPanel();
        inputFieldPanel.setLayout(null);
        inputFieldPanel.setPreferredSize(new Dimension(510, 42));
        inputFieldPanel.setMinimumSize(new Dimension(510, 10));
        inputFieldPanel.setName("");
        return inputFieldPanel;
    }

    private void evaluate() {
        try {
            final RomanCalculation calculator = new RomanCalculation();
            calculator.calculate(inputField.getText());
            final String decimalCalculation = calculator.getDecimalCalculation();
            final String romanResult = calculator.getRomanResult();
            displayResults(decimalCalculation, romanResult);
        } catch (IllegalArgumentException | AnachronisticZeroException anException) {
            displayInvalidNumberMessage(anException);
        }
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialise() {
        initialiseFrame();
        final JPanel outerPanel = initialiseOuterPanel();
        initialiseResultField(initialiseResultFieldPanel(outerPanel));
        initialiseInputField(initialiseInputFieldPanel(outerPanel));
        populateNumbersPanel(initialiseNumbersPanel(outerPanel));
        populateOperatorsPanel(initialiseOperatorsPanel(outerPanel));
        initialiseEqualsButton(initialiseEqualsPanel(outerPanel));
        initialiseClearButton(initialiseClearPanel(outerPanel));
    }

    private void initialiseClearButton(final JPanel clearPanel) {
        final JButton clearButton = new JButton("C");
        clearButton.addActionListener(event -> clearEverything());
        clearButton.setFont(new Font(FONT_NAME, Font.BOLD, 30));
        clearButton.setForeground(Color.RED);
        clearPanel.add(clearButton);
    }

    private JPanel initialiseClearPanel(final JPanel outerPanel) {
        final JPanel clearPanel = new JPanel();
        clearPanel.setBounds(344, 278, 183, 62);
        outerPanel.add(clearPanel);
        clearPanel.setLayout(new GridLayout(0, 1, 0, 0));
        return clearPanel;
    }

    private void initialiseDecimalField(final JPanel decimalPanel) {
        decimalField = doCommonFieldInitialisation();
        decimalField.setHorizontalAlignment(SwingConstants.CENTER);
        decimalField.setBackground(new Color(240, 240, 240));
        decimalPanel.add(decimalField);
    }

    private JPanel initialiseDecimalFieldPanel(final JPanel outerPanel) {
        final JPanel decimalPanel = doCommonFieldPanelInitialisation();
        decimalPanel.setBounds(17, 127, 510, 42);
        outerPanel.add(decimalPanel);
        return decimalPanel;
    }

    private void initialiseEqualsButton(final JPanel equalsPanel) {
        final JButton equalsButton = new JButton("=");
        equalsButton.addActionListener(event -> evaluate());
        equalsButton.setFont(new Font(FONT_NAME, Font.BOLD, 30));
        equalsPanel.add(equalsButton);
    }

    private JPanel initialiseEqualsPanel(final JPanel outerPanel) {
        final JPanel equalsPanel = new JPanel();
        equalsPanel.setBounds(344, 489, 183, 62);
        outerPanel.add(equalsPanel);
        equalsPanel.setLayout(new GridLayout(0, 1, 0, 0));
        return equalsPanel;
    }

    private void initialiseFrame() {
        romanCalculatorFrame = new JFrame();
        romanCalculatorFrame.setTitle("Roman Calculator");
        romanCalculatorFrame.setBounds(650, 250, 560, 600);
        romanCalculatorFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void initialiseInputButton(final JPanel aPanel, final String text) {
        final JButton aButton = new JButton(text);
        aButton.addActionListener(event -> inputButtonAction(aButton));
        aButton.setFont(new Font(FONT_NAME, Font.BOLD, 30));
        aPanel.add(aButton);
    }

    private void initialiseInputField(final JPanel inputFieldPanel) {
        inputField = doCommonFieldInitialisation();
        inputField.setHorizontalAlignment(SwingConstants.RIGHT);
        inputFieldPanel.add(inputField);
    }

    private JPanel initialiseInputFieldPanel(final JPanel outerPanel) {
        final JPanel inputFieldPanel = doCommonFieldPanelInitialisation();
        inputFieldPanel.setBounds(17, 74, 510, 42);
        outerPanel.add(inputFieldPanel);
        return inputFieldPanel;
    }

    private JPanel initialiseNumbersPanel(final JPanel outerPanel) {
        final JPanel numbersPanel = new JPanel();
        numbersPanel.setBounds(17, 258, 300, 300);
        outerPanel.add(numbersPanel);
        numbersPanel.setLayout(new GridLayout(3, 3, 0, 0));
        return numbersPanel;
    }

    private JPanel initialiseOperatorsPanel(final JPanel outerPanel) {
        final JPanel operatorsPanel = new JPanel();
        operatorsPanel.setBounds(354, 351, 165, 127);
        outerPanel.add(operatorsPanel);
        operatorsPanel.setLayout(new GridLayout(2, 2, 0, 0));
        return operatorsPanel;
    }

    private JPanel initialiseOuterPanel() {
        final JPanel outerPanel = new JPanel();
        romanCalculatorFrame.getContentPane().add(outerPanel, BorderLayout.CENTER);
        outerPanel.setLayout(null);
        final JPanel decimalPanel = initialiseDecimalFieldPanel(outerPanel);
        initialiseDecimalField(decimalPanel);
        return outerPanel;
    }

    private void initialiseResultField(final JPanel resultFieldPanel) {
        resultField = doCommonFieldInitialisation();
        resultField.setHorizontalAlignment(SwingConstants.CENTER);
        resultFieldPanel.add(resultField);
    }

    private JPanel initialiseResultFieldPanel(final JPanel outerPanel) {
        final JPanel resultFieldPanel = doCommonFieldPanelInitialisation();
        resultFieldPanel.setBounds(17, 21, 510, 42);
        outerPanel.add(resultFieldPanel);
        return resultFieldPanel;
    }

    private void initialiseSpacerPanel(final JPanel numbersPanel) {
        final JPanel spacerPanel = new JPanel();
        numbersPanel.add(spacerPanel);
    }

    private void inputCharacterFrom(final JButton aButton) {
        if (isOperator(aButton) && operatorPresentAtEnd) {
            if (" - ".equals(aButton.getText()) && !nextIsNegative) {
                inputField.setText(inputField.getText() + "-");
                nextIsNegative = true;
            }
        } else {
            inputField.setText(inputField.getText() + aButton.getText());
            nextIsNegative = false;
        }
    }

    private void inputButtonAction(final JButton aButton) {
        inputCharacterFrom(aButton);
        if (isOperator(aButton)) {
            operatorPresentAtEnd = true;
        } else {
            operatorPresentAtEnd = false;
        }
    }

    private boolean isOperator(final JButton aButton) {
        return OPERATORS.contains(aButton.getText().trim());
    }

    private void populateNumbersPanel(final JPanel numbersPanel) {
        for (final String aCharacter : RomanNumeral.getBasicNumerals()) {
            if (I.getRoman().equals(aCharacter)) {
                initialiseSpacerPanel(numbersPanel);
            }
            initialiseInputButton(numbersPanel, aCharacter);
        }
    }

    private void populateOperatorsPanel(final JPanel operatorsPanel) {
        for (final String anOperator : OPERATORS) {
            initialiseInputButton(operatorsPanel, " " + anOperator + " ");
        }
    }
}
