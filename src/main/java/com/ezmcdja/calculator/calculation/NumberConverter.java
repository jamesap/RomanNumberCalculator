package com.ezmcdja.calculator.calculation;

import static com.ezmcdja.calculator.calculation.RomanNumeral.I;
import static com.ezmcdja.calculator.calculation.RomanNumeral.IV;
import static com.ezmcdja.calculator.calculation.RomanNumeral.V;
import static com.ezmcdja.calculator.calculation.RomanNumeral.IX;
import static com.ezmcdja.calculator.calculation.RomanNumeral.X;
import static com.ezmcdja.calculator.calculation.RomanNumeral.XL;
import static com.ezmcdja.calculator.calculation.RomanNumeral.L;
import static com.ezmcdja.calculator.calculation.RomanNumeral.XC;
import static com.ezmcdja.calculator.calculation.RomanNumeral.C;
import static com.ezmcdja.calculator.calculation.RomanNumeral.CD;
import static com.ezmcdja.calculator.calculation.RomanNumeral.D;
import static com.ezmcdja.calculator.calculation.RomanNumeral.CM;
import static com.ezmcdja.calculator.calculation.RomanNumeral.M;

/**
 * Author: James McDermott.
 */
public class NumberConverter {
    public String convertToRoman(final Integer aNumber) {
        Integer decimalVersion = aNumber;
        boolean isNegative = false;
        if (decimalVersion < 0) {
            isNegative = true;
            decimalVersion *= -1;
        }

        String converted = decomposeDecimal(decimalVersion, isNegative);
        converted = tidyUpRomanNumber(converted);
        return converted;
    }

    private String decomposeDecimal(Integer decimalVersion, final boolean isNegative) {
        final StringBuilder builder = new StringBuilder();
        final RomanNumeral[] numeralsUsedInConversion = new RomanNumeral[] { D, L, V, I };
        for (final RomanNumeral each : numeralsUsedInConversion) {
            while (decimalVersion >= each.getDecimal()) {
                builder.append(each.getRoman());
                decimalVersion -= each.getDecimal();
            }
        }
        if (isNegative) {
            builder.insert(0, "-");
        }
        return builder.toString();
    }

    public Integer parseToDecimal(final String romanVersion) {
        final StringBuilder builder = new StringBuilder(romanVersion);
        final Boolean isNegative = builder.charAt(0) == '-';
        Integer converted = 0;

        converted = parseRoman(builder);

        if (isNegative) {
            converted *= -1;
        }
        return converted;
    }

    private Integer parseRoman(final StringBuilder builder) {
        Integer converted = 0;
        builder.reverse();
        while (builder.length() != 0) {
            converted = parseCharacter(builder, converted);
        }
        return converted;
    }

    private Integer parseCharacter(final StringBuilder builder, Integer converted) {
        final Character each = builder.charAt(0);
        final Integer parsed = RomanNumeral.parseNumeralToDecimal(new String(new char[] { each }));

        converted += parsed;
        if (builder.length() > 1) {
            converted = checkNextCharacter(builder, converted, parsed);
        }
        builder.deleteCharAt(0);
        return converted;
    }

    private Integer checkNextCharacter(final StringBuilder builder, Integer converted, final Integer parsed) {
        final Character next;
        final Integer parsedNext;
        next = builder.charAt(1);
        parsedNext = RomanNumeral.parseNumeralToDecimal(new String(new char[] { next }));
        if (parsedNext < parsed) {
            converted -= parsedNext;
            builder.deleteCharAt(1);
        }
        return converted;
    }

    String tidyUpRomanNumber(final String toTidy) {
        String tidying = toTidy;
        tidying = collapseDs(tidying);
        tidying = collapseLs(tidying);
        tidying = collapseVs(tidying);
        tidying = collapseSubtractionCases(tidying);
        return tidying;
    }

    private String collapseDs(final String toTidy) {
        return toTidy.replaceAll("DD", M.getRoman());
    }

    private String collapseLs(final String toTidy) {
        return toTidy.replaceAll("LL", C.getRoman());
    }

    private String collapseSubtractionCases(final String toTidy) {
        String tidying = toTidy.replaceAll("IIII", IV.getRoman());
        tidying = tidying.replaceAll("VIV", IX.getRoman());
        tidying = tidying.replaceAll("XXXX", XL.getRoman());
        tidying = tidying.replaceAll("LXL", XC.getRoman());
        tidying = tidying.replaceAll("CCCC", CD.getRoman());
        tidying = tidying.replaceAll("DCD", CM.getRoman());
        return tidying;
    }

    private String collapseVs(final String toTidy) {
        return toTidy.replaceAll("VV", X.getRoman());
    }
}
