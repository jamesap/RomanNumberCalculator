package com.ezmcdja.calculator.calculation;

/**
 * A custom exception, used when the calculator encounters a zero result.
 */
public class AnachronisticZeroException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AnachronisticZeroException(final String message) {
        super(message);
    }
}
