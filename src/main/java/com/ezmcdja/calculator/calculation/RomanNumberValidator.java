package com.ezmcdja.calculator.calculation;

import static com.ezmcdja.calculator.calculation.RomanNumeral.IV;
import static com.ezmcdja.calculator.calculation.RomanNumeral.IX;
import static com.ezmcdja.calculator.calculation.RomanNumeral.XC;
import static com.ezmcdja.calculator.calculation.RomanNumeral.XL;
import static com.ezmcdja.calculator.calculation.RomanNumeral.CD;
import static com.ezmcdja.calculator.calculation.RomanNumeral.CM;

import java.util.Arrays;
import java.util.List;

/**
 * Author: James McDermott.
 */
public class RomanNumberValidator {
    private final NumberConverter converter;

    public RomanNumberValidator(final NumberConverter converter) {
        this.converter = converter;
    }

    public void validateRomanNumber(final String aNumber) {
        final String toValidate = converter.tidyUpRomanNumber(aNumber);

        for (int currentIndex = 0; currentIndex < toValidate.length(); currentIndex++) {
            final Character current = toValidate.charAt(currentIndex);
            if (numberIsNegative(current)) {
                continue;
            }
            checkNextCharacterIfPresent(aNumber, toValidate, currentIndex, current);
            parseAndValidateNumeral(current);
        }
    }

    private void checkNextCharacterIfPresent(final String aNumber, final String toValidate, final int currentIndex, final Character current) {
        if (currentIndex + 1 < toValidate.length()) {
            final Character next = toValidate.charAt(currentIndex + 1);
            final boolean charsInOrder = charsInOrder(current, next);

            final String nextCharacterChecking = new String(new char[] { current, next });
            checkNumberValidity(aNumber, charsInOrder, nextCharacterChecking);
        }
    }

    private boolean numberIsNegative(final Character current) {
        return current == '-';
    }

    private void checkNumberValidity(final String aNumber, final boolean charsInOrder, final String nextCharacterChecking) {
        final List<String> validWrongOrderCombinations = Arrays
                .asList(IV.getRoman(), IX.getRoman(), XL.getRoman(), XC.getRoman(), CD.getRoman(), CM.getRoman());
        if (isValidRomanNumber(charsInOrder, validWrongOrderCombinations, nextCharacterChecking)) {
            throw new IllegalArgumentException("INVALID NUMBER: " + aNumber);
        }
    }

    private boolean charsInOrder(final Character current, final Character next) {
        return parseAndValidateNumeral(current) >= parseAndValidateNumeral(next);
    }

    private boolean isValidRomanNumber(final boolean charsInOrder, final List<String> validWrongOrderCombinations,
                                       final String nextCharacterChecking) {
        return !charsInOrder && !validWrongOrderCombinations.contains(nextCharacterChecking);
    }

    private Integer parseAndValidateNumeral(final Character numeral) {
        final Integer decimalVersion = RomanNumeral.parseNumeralToDecimal(new String(new char[] { numeral }));
        if (decimalVersion == 0) {
            throw new IllegalArgumentException("INVALID NUMBER: " + numeral);
        }
        return decimalVersion;
    }
}
