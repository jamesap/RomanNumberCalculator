Feature: 'Addition, subtraction, multiplication, and division of Roman numbers'

  Background: 
    Given a RomanCalculation to be used in testing

  Scenario Outline: Valid operations
    Given I have a mathematical expression in Roman numbers "<inputExpression>"
     When the expression is fed into the calculation
     Then the calculation runs with no exception
      And the returned result equals the expected "<result>"
      And the returned decimal equivalent equals the expected "<decimalCalc>"

    Examples: Test data: addition, subtraction, multiplication, and division

      | inputExpression                     | result                                                     | decimalCalc                             |
      #addition
      | I + I                               | II                                                         | (1 + 1 = 2)                             |
      | II + II                             | IV                                                         | (2 + 2 = 4)                             |
      | I + I + I                           | III                                                        | (1 + 1 + 1 = 3)                         |
      | IV + LI + CCCLXXV + DXXXIII         | CMLXIII                                                    | (4 + 51 + 375 + 533 = 963)              |
      | MMMDCCCLXXII + MMMMMMMDCCCLXXIII    | MMMMMMMMMMMDCCXLV                                          | (3872 + 7873 = 11745)                   |
      | XCI + LXXVII                        | CLXVIII                                                    | (91 + 77 = 168)                         |
      | CXLIII + CCCXIV                     | CDLVII                                                     | (143 + 314 = 457)                       |
      | I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I + I | XXX | (1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 = 30) |
      #subtraction
      | -II + XIV                           | XII                                                        | (-2 + 14 = 12)                          |
      | II - I                              | I                                                          | (2 - 1 = 1)                             |
      | IV - II                             | II                                                         | (4 - 2 = 2)                             |
      | II - IV                             | -II                                                        | (2 - 4 = -2)                            |
      | VI - II - II                        | II                                                         | (6 - 2 - 2 = 2)                         |
      | XXX + X - X                         | XXX                                                        | (30 + 10 - 10 = 30)                     |
      | XXX - X + X                         | XXX                                                        | (30 - 10 + 10 = 30)                     |
      | XXX - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I                     | I   | (30 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 = 1) |
      | XXX - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I - I | -IV | (30 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 = -4) |
      #multiplication
      | I x I                               | I                                                          | (1 x 1 = 1)                             |
      | I x II                              | II                                                         | (1 x 2 = 2)                             |
      | II x II                             | IV                                                         | (2 x 2 = 4)                             |
      | XI x XII                            | CXXXII                                                     | (11 x 12 = 132)                         |
      | I x II x III                        | VI                                                         | (1 x 2 x 3 = 6)                         |
      | IV x IX x X                         | CCCLX                                                      | (4 x 9 x 10 = 360)                      |
      | X x X x X                           | M                                                          | (10 x 10 x 10 = 1000)                   |
      | I x -I                              | -I                                                         | (1 x -1 = -1)                           |
      | -I x I                              | -I                                                         | (-1 x 1 = -1)                           |
      | -I x -I                             | I                                                          | (-1 x -1 = 1)                           |
      | -XCI x -XII x XXIX                  | MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMDCLXVIII                    | (-91 x -12 x 29 = 31668)                |
      | LXXXIII x X x LXVII                 | MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMDCX | (83 x 10 x 67 = 55610)                  |
      #division
      | I ÷ I                               | I                                                          | (1 ÷ 1 = 1)                             |
      | I ÷ -I                              | -I                                                         | (1 ÷ -1 = -1)                           |
      | -I ÷ -I                             | I                                                          | (-1 ÷ -1 = 1)                           |
      | IV ÷ II                             | II                                                         | (4 ÷ 2 = 2)                             |
      | V ÷ II                              | II                                                         | (5 ÷ 2 = 2) (Remainder: 1)              |
      | XX ÷ V ÷ II                         | II                                                         | (20 ÷ 5 ÷ 2 = 2)                        |
      | XX ÷ X ÷ II                         | I                                                          | (20 ÷ 10 ÷ 2 = 1)                       |
      #mixed operations
      | X ÷ V + I                           | III                                                        | (10 ÷ 5 + 1 = 3)                        |
      | X + V x II                          | XX                                                         | (10 + 5 x 2 = 20)                       |
      | V + III x IV - III                  | XIV                                                        | (5 + 3 x 4 - 3 = 14)                    |
      | XXX - X + X - X + X                 | XXX                                                        | (30 - 10 + 10 - 10 + 10 = 30)           |
      | XXX + X - X + X - X                 | XXX                                                        | (30 + 10 - 10 + 10 - 10 = 30)           |
      | MMCDVIII - MMMMMMCCCXVII            | -MMMCMIX                                                   | (2408 - 6317 = -3909)                   |
      | -X - C - C + C + X                  | -C                                                         | (-10 - 100 - 100 + 100 + 10 = -100)     |
      | CXVII - CCCXCVIII + LXXVI + CCX     | V                                                          | (117 - 398 + 76 + 210 = 5)              |
      | MDLXXVI x DXI ÷ CV + DX - III x III | MMMMMMMMCLXX                                               | (1576 x 511 ÷ 105 + 510 - 3 x 3 = 8170) |
      #single numbers
      | I                                   | I                                                          | (1 = 1)                                 |
      | V                                   | V                                                          | (5 = 5)                                 |
      | DCLXVI                              | DCLXVI                                                     | (666 = 666)                             |
      | MMMMMMMMMCCCXLIII                   | MMMMMMMMMCCCXLIII                                          | (9343 = 9343)                           |

  Scenario Outline: Invalid operations - non-Roman numbers
    Given I have a mathematical expression with invalid Roman numbers "<inputExpression>"
     When the expression is fed into the calculation
     Then an IllegalArgumentException is thrown with message "<message>"

    Examples: Sample data with invalid Roman numbers
      | inputExpression | message                |
      | IXD             | INVALID NUMBER: IXD    |
      | IMD             | INVALID NUMBER: IMD    |
      | IMX             | INVALID NUMBER: IMX    |
      | VIM             | INVALID NUMBER: VIM    |
      | ID              | INVALID NUMBER: ID     |
      | IM              | INVALID NUMBER: IM     |
      | IC              | INVALID NUMBER: IC     |
      | XD              | INVALID NUMBER: XD     |
      | XM              | INVALID NUMBER: XM     |
      | DM              | INVALID NUMBER: DM     |
      | CLDXVI          | INVALID NUMBER: CLDXVI |

  Scenario Outline: Invalid operations - zero results
    Given I have a mathematical expression that will result in zero "<inputExpression>"
     When the expression is fed into the calculation
     Then an AnachronisticZeroException is thrown with message 'ZERO RESULT! ROME PANICS!'

    Examples: Sample data that evaluates to zero
      | inputExpression |
      | I - I           |
      | -I + I          |
      | XV - V - V - V  |
      | -C + D - CD     |
