package com.ezmcdja.calculator;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * Test to run {@link RomanCalculationSteps}.
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty" }, features = { "src/test/cucumber/com/ezmcdja/calculator/roman-calculation.feature" })
public class RomanCalculationTest {
}
