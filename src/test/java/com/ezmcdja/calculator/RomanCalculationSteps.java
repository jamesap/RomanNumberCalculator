package com.ezmcdja.calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.ezmcdja.calculator.calculation.RomanCalculation;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Cucumber test steps for {@link RomanCalculation}.
 */
public class RomanCalculationSteps {
    private RomanCalculation calculator;
    private String inputExpression;
    private Exception caughtException;

    @Given("^a RomanCalculation to be used in testing$")
    public void a_RomanCalculation_to_be_used_in_testing() throws Throwable {
        calculator = new RomanCalculation();
        caughtException = null;
    }

    @Given("^I have a mathematical expression in Roman numbers \"([^\"]*)\"$")
    public void i_have_a_mathematical_expression_in_Roman_numbers(final String inputExpression) throws Throwable {
        this.inputExpression = inputExpression;
    }

    @Given("^I have a mathematical expression with invalid Roman numbers \"([^\"]*)\"$")
    public void i_have_a_mathematical_expression_with_invalid_Roman_numbers(final String inputExpression) throws Throwable {
        this.inputExpression = inputExpression;
    }

    @Given("^I have a mathematical expression that will result in zero \"([^\"]*)\"$")
    public void i_have_a_mathematical_expression_that_will_result_in_zero(final String inputExpression) throws Throwable {
        this.inputExpression = inputExpression;
    }

    @When("^the expression is fed into the calculation$")
    public void the_expression_is_fed_into_the_calculation() throws Throwable {
        try {
            calculator.calculate(inputExpression);
        } catch (final Exception ex) {
            caughtException = ex;
        }
    }

    @Then("^the calculation runs with no exception$")
    public void the_calculation_runs_with_no_exception() throws Throwable {
        assertNull(caughtException);
    }

    @Then("^the returned result equals the expected \"([^\"]*)\"$")
    public void the_returned_result_equals_the_expected(final String expectedResult) throws Throwable {
        assertEquals(expectedResult, calculator.getRomanResult());
    }

    @Then("^the returned decimal equivalent equals the expected \"([^\"]*)\"$")
    public void the_returned_decimal_equivalent_equals_the_expected(final String decimalCalc) throws Throwable {
        assertEquals(decimalCalc, calculator.getDecimalCalculation());
    }

    @Then("^an IllegalArgumentException is thrown with message \"([^\"]*)\"$")
    public void an_IllegalArgumentException_is_thrown_with_message(final String exceptionMessage) throws Throwable {
        assertEquals("IllegalArgumentException", caughtException.getClass().getSimpleName());
        assertEquals(exceptionMessage, caughtException.getMessage());
    }

    @Then("^an AnachronisticZeroException is thrown with message 'ZERO RESULT! ROME PANICS!'$")
    public void an_AnachronisticZeroException_is_thrown_with_message_Zero_Result_Rome_Panics() throws Throwable {
        assertEquals("AnachronisticZeroException", caughtException.getClass().getSimpleName());
        assertEquals("ZERO RESULT! ROME PANICS!", caughtException.getMessage());
    }
}
