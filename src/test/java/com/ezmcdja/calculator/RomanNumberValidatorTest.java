package com.ezmcdja.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.ezmcdja.calculator.calculation.NumberConverter;
import com.ezmcdja.calculator.calculation.RomanNumberValidator;
import com.ezmcdja.calculator.gui.CalculatorWindow;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

/**
 * Tests for {@link RomanNumberValidator}.
 */
@RunWith(JUnitParamsRunner.class)
public class RomanNumberValidatorTest {
    private RomanNumberValidator validator;
    private NumberConverter converter;

    @Before
    public void setUp() {
        converter = new NumberConverter();
        validator = new RomanNumberValidator(converter);
    }

    @Test
    public void testValidation() {
        String toValidate = "";
        for (int i = 1; i <= 1000; i++) {
            toValidate = converter.convertToRoman(i);
            validator.validateRomanNumber(toValidate);
        }
    }

    @Test
    @Parameters({ "w", "F", "wibby", "WRONG NUMBER", "DCCMIIXVL", "IL", "IC", "IM" })
    public void testInvalidation(final String toValidate) {
        new CalculatorWindow();
        try {
            validator.validateRomanNumber(toValidate);
        } catch (final Exception ex) {
            assertEquals("IllegalArgumentException", ex.getClass().getSimpleName());
        }
    }

    @AfterClass
    public static void testWibby() {
        new RomanCalculationTest();
        new CalculatorWindow();
        CalculatorWindow.main(new String[0]);
    }

}
